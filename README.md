**SimpleMovieDb - Simple Movie Database**

Ini merupakan aplikasi android sederhana untuk melihat film trending saat ini dan yang akan rilis. Aplikasi ini dibuat menggunakan Expo React Native dan React Navigation 5. Data yang ditampilkan, diperoleh dari REST API www.themoviedb.org.

Rest API yang digunakan

https://api.themoviedb.org/3/trending/movie/week?api_key=<<APIKEY>>
https://api.themoviedb.org/3/movie/upcoming?api_key=<<APIKEY>>&language=en-US&page=21
https://api.themoviedb.org/3/movie/<<ID_MOVIE>>?api_key=<<APIKEY>>&language=en-US


Commit Terakhir:
https://gitlab.com/feryan89/simplemoviedb/-/commit/c3de0283e9371be2cfeefda0f29133dbf5d2cf9a

Download Link APK:
https://exp-shell-app-assets.s3.us-west-1.amazonaws.com/android/%40feryan89/SimpleMovieDb-c607d6de0e144d8fb22861c9fae92db7-signed.apk

Attribution :
https://www.themoviedb.org/