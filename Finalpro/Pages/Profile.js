import React from 'react'
import { StyleSheet, Text, View, Image } from 'react-native'

const Profile = ({navigation}) => {
    return (
        <View style={styles.container}>
            <Text style={{fontSize:18,fontWeight:"bold"}}>About Us</Text>
            <Text style={{marginTop:10}}>MovieDb is simple app that provides info about movies. The database using API from www. themoviedb.org. So, you must connect internet before open this app.</Text>
            <Text style={{marginTop:10}}>This App was built to apply final project of SanberCode</Text>
            <Text style={{fontSize:18,fontWeight:"bold", marginTop:10}}>About Me</Text>
            <Text style={{marginTop:10}}>I'm Fatchurrohman Feryanto, freelance programmer from Ponorogo.</Text>
            <Text style={{fontSize:18,fontWeight:"bold", marginTop:10}}>Attribution to:</Text>
            <Image style={{width: 223, height: 173, marginTop:10}}source={require('../images/tmdb.jpg')} resizeMode="contain"/>
        </View>
    )
}

export default Profile

const styles = StyleSheet.create({

})
