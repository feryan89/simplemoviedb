import { NavigationContainer } from '@react-navigation/native';
import React from 'react';
import {
    Platform,View, Text, Image, StyleSheet,TouchableOpacity
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';

export default class FilmItem extends React.Component{
 render(){
    let film=this.props.film;
    let navigation=this.props.navigation;
    let tahunRilis=film.release_date;
    tahunRilis=tahunRilis.substring(0,4);
    let getGenre=film.genre_ids;
    let tmpGenre="";
    getGenre.forEach((item, index, arr) => {
        if(index<getGenre.length-1){
            tmpGenre=tmpGenre+cekGenre(item)+", ";
        }else{
            tmpGenre=tmpGenre+cekGenre(item)
        }
    });
    getGenre.join("-");
     
    function cekGenre(genre) {
        switch(genre){
            case 28:
                genre= "Action";
                break;
            case 12:
                genre="Adventure";
                break;
            case 16:
                genre="Animation";
                break;
            case 35:
                genre= "Comedy";
                break;
            case 80:
                genre="Crime";
                break;
            case 99:
                genre="Documentary";
                break;
            case 18:
                genre= "Drama";
                break;
            case 10751:
                genre="Family";
                break;
            case 14:
                genre="Fantasy";
                break;
            case 36:
                genre= "History";
                break;
            case 27:
                genre="Horror";
                break;
            case 10402:
                genre="Music";
                break;
            case 9648:
                genre= "Mystery";
                break;
            case  10749:
                genre="Romance";
                break;
            case 878:
                genre= "Science Fiction";
                break;
            case 10770:
                genre="TV Movie";
                break;
            case  53:
                genre="Thriller";
                break;
            case 10752:
                genre= "War";
                break;
            case  37:
                genre="Western";
                break;
            default :
                genre=genre;
                break;

        }
        return genre;
    }
   
   

     return(
            <View style={styles.container} >
                <TouchableOpacity onPress={()=>navigation.push("Detail",{ id: film.id }) }>
                <View style={styles.listData}>
                    
                        <Image source={{uri:'https://image.tmdb.org/t/p/w220_and_h330_face/'+`${film.poster_path}`}} style={{height:80,width:50}} resizeMode="contain"/>
                        <View style={styles.vListContainer}>
                            <Text style={{fontSize:18}}>{film.title} ({tahunRilis})</Text>
                            <View style={styles.vListStar}>
                                <Icon name="star" size={20} color="red"/>
                                <Text style={{marginLeft:5}}>{film.vote_average}</Text>
                            </View>
                            <Text>{tmpGenre}</Text>
                        </View>
                    
                </View>
                </TouchableOpacity>
            </View>
     );
 }
}

const styles = StyleSheet.create({
    container:{
        flex:1,
    },
    listData:{
        flexDirection:'row'
        
      },
      vListContainer:{
            flexDirection:'column'
      },
      vListStar:{
        flexDirection:'row'
      },
})


//<Image source={{uri:video.snippet.thumbnails.medium.url}} style={{height:200}}/>

//