import React, { useEffect, useState } from 'react'
import { StyleSheet, Text, View, Image, TouchableOpacity,FlatList, ActivityIndicator, } from 'react-native'
import data from '../detail.json';
import Icon from 'react-native-vector-icons/MaterialIcons'; 

const Detail = ({route}) => {
    const [isLoading, setLoading] = useState(true);
    const [data, setData] = useState([]);
    //const [dataG, setDataG] = useState([]);
   
    useEffect(() => {

        fetch("https://api.themoviedb.org/3/movie/"+`${route.params.id}`+"?api_key=659bb1574dddd94cbbe15920e9636d0b&language=en-US")
      //fetch('https://api.themoviedb.org/3/trending/movie/week?api_key=659bb1574dddd94cbbe15920e9636d0b')
        .then((response) => response.json())
        .then((json) => setData(json))
        .catch((error) => console.error(error))
        .finally(() => setLoading(false));
    }, []);

    return (
        <View style={styles.container}>
                {isLoading ? <ActivityIndicator/> : (
                    <View style={styles.Konten}>
                            <Image source={{uri:'https://image.tmdb.org/t/p/w1920_and_h800_multi_faces'+`${data.backdrop_path}`}} style={{height:170,width:'100%'}} resizeMode="contain"/>
                            <View style={styles.vPostInfo}>
                                <Image source={{uri:'https://image.tmdb.org/t/p/w220_and_h330_face'+`${data.poster_path}`}} style={{height:160,width:100}} resizeMode="contain"/>
                                <View style={styles.vInfo}>
                                    <Text style={{fontSize:20,fontWeight:"bold", marginBottom:10}}>
                                        {data.title} ({data.release_date.substr(0,4)})
                                    </Text>
                                    <Text style={{ marginBottom:5}}>
                                        {data.genres.map((element,index)=> (index<data.genres.length-1)? element.name+", ":element.name)}
                                    </Text>
                                    <Text style={{fontStyle:'italic', marginBottom:5}}>
                                        {data.tagline}
                                    </Text>
                                    <Text style={{ marginBottom:5}}>
                                        Release : {data.release_date}
                                    </Text>
                                    <View style={{flexDirection:'row',justifyContent:'flex-start', marginBottom:5}}>
                                        <Icon name="star" size={20} color="red"/>
                                        <Text style={{marginLeft:10,fontWeight:'bold'}}>{data.vote_average}</Text>
                                    </View>
                                </View>
                            </View>
                            <View>
                                    <Text style={{fontSize:20,fontWeight:'bold',marginVertical:10}}>
                                        Overview
                                    </Text>
                                    <Text>
                                         {data.overview }
                                    </Text>
                            </View>
                    </View>
                    )}
        </View>
    )
}

export default Detail

const styles = StyleSheet.create({
    container:{
        flex:1,
    },
    Konten:{
       marginHorizontal:10,     
       flexDirection:'column'
    },
    vPostInfo:{
        flexDirection:'row',
        justifyContent:'flex-start',
    },
    vInfo:{
        marginLeft:10,
        flexDirection:'column',
        justifyContent:'flex-start',

    }

})
//<Image source={{uri:'https://image.tmdb.org/t/p/w1920_and_h800_multi_faces/'+`${data.backdrop_path}`}} style={{height:150,width:50}} resizeMode="contain"/>