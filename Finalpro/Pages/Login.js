import React from 'react'
import { StyleSheet, Text, View,TouchableOpacity, TextInput,Image } from 'react-native'

const Login = ({navigation}) => {
    return (
        <View style={styles.container}> 
            <View style={styles.titleLogo}>
                <Image source={require('../images/logo2.png')} style={{height:80,width:275}} resizeMode="contain"/>
            </View>
            <View style={{marginHorizontal:20}}>
                <View style={styles.subTitleLogin}>
                    <Text style={{fontSize:18,color:'#003366', fontWeight:"bold"}}>Login</Text>
                </View>
                <View style={styles.formLogin}> 
                    <View style={styles.ViewInputUser}>
                        <Text style={{color:'#003366',fontSize:14}}>Username</Text>
                        <TextInput style={styles.txtInput} />
                    </View>
                    <View style={styles.ViewInputUser}>
                        <Text  style={{color:'#003366',fontSize:14}}>Password</Text>
                        <TextInput style={styles.txtInput} secureTextEntry={true}  />
                    </View>
                </View>
                <View style={styles.ViewbtnLogin}>
                    <View style={styles.Viewbtn}>
                        <TouchableOpacity style={[{backgroundColor:'#3EC6FF'},styles.btnLogin]} onPress={() => navigation.push("App")} ><Text style={{ color:'white'}}>Login</Text></TouchableOpacity>
                    </View>
                </View>
            </View>
        </View>
    )
}

export default Login

const styles = StyleSheet.create({
    container:{
        flex:1,
    },
    titleLogo:{
        //marginTop:15,
        height:150,
        alignItems:'center',
        justifyContent: 'center',
        backgroundColor:'#000',
    },
    subTitleLogin:{
        height:50,
        alignItems:'center',
        justifyContent: 'center'
    },
    formLogin:{
        width:"100%",
        flexDirection:'column',
        height:130,
        alignItems:'center',
    },
    ViewbtnLogin:{
        marginTop:10,
        height:130,
        flexDirection:'column',
        alignItems:'center',
        justifyContent: 'space-around',
    },
    Viewbtn:{
        width:"100%",
    },
    ViewInputUser:{
        width:"100%",
        marginTop:10
    },
    txtInput:{
        fontSize:14,
        height:35,
        borderWidth:1,
        borderColor:'#003366',
        borderColor:"#CCC",
        borderWidth:2,
    },
   btnLogin:{
        width:"100%",
        color:"#fff",
        backgroundColor:"#FF0000",
        paddingTop:10,
        paddingBottom:10,
        alignItems:'center'
   },
})
//<TouchableOpacity onPress={() => navigation.push("App")}><Text style={{ color:'black'}}>SignIn</Text></TouchableOpacity>