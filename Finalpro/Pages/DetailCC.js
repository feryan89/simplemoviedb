import React, { useEffect, useState } from 'react'
import { StyleSheet, Text, View, TouchableOpacity,FlatList, ActivityIndicator, } from 'react-native'

const Detail = ({route}) => {
    const [isLoading, setLoading] = useState(true);
    const [data, setData] = useState([]);
    //const [dataG, setDataG] = useState([]);
    

    useEffect(() => {

        fetch("https://api.themoviedb.org/3/movie/"+`${route.params.id}`+"?api_key=659bb1574dddd94cbbe15920e9636d0b&language=en-US")
      //fetch('https://api.themoviedb.org/3/trending/movie/week?api_key=659bb1574dddd94cbbe15920e9636d0b')
        .then((response) => response.json())
        .then((json) => setData(json.genres))
        .catch((error) => console.error(error))
        .finally(() => setLoading(false));
    }, []);
    
   
    return (
        <View>
            <Text>Hello </Text>
            <TouchableOpacity onPress={() =>navigation.toggleDrawer()}><Text style={{ color:'red'}}>Drawer</Text></TouchableOpacity>
            <View style={{ flex: 1, padding: 24 }}>
                {isLoading ? <ActivityIndicator/> : (
                    <View style={styles.Konten}>
                        <View>
                            
                                <Text>{data.map((element,index)=> (index<data.length-1)? element.name+", ":element.name)}</Text>
                        </View>
                    </View>
                    )}
            </View>
        </View>
    )
}

export default Detail

const styles = StyleSheet.create({})
