import React, {useEffect} from 'react'
import { Image, StyleSheet, Text, View } from 'react-native'

const Splash = ({navigation}) => {
    //logic untuk membuat splash screen 
    //dan akan langsung mengarah ke halaman login
    useEffect(()=>{
        setTimeout(()=>{
          navigation.replace('Login')// jika dirubah ke Register akan ke navigate ke Regsiter
        }, 1000) // untuk mengubah 3 detik menuju halaman berikutnya 
      },[])
    //router langsung menuju ke login
    return (
        <View style={styles.container}>
            <View style={styles.containerWrapper}>
                <Image style={{width: 223, height: 133}}source={require('../images/logo.png')} resizeMode="contain"/>
            </View>
        </View>
    )
}

export default Splash

const styles = StyleSheet.create({
    container:{
        flex: 1,
        justifyContent:'center',
        alignItems:'center',
        backgroundColor:"#000"
    },
    containerWrapper:{
        width: 300,
        height:300,
        backgroundColor:'#000',
        justifyContent:'center',
        alignItems:'center'
    }
})
