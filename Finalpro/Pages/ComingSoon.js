import React, { useEffect, useState } from 'react';
import { StyleSheet, Text, View, TouchableOpacity, FlatList, ActivityIndicator, } from 'react-native';
import FilmItem from './Component/filmItems';
import VideoItem from './Component/videoitems';
//import data from '../data.json';


const ComingSoon = ({navigation}) => {
    const [isLoading, setLoading] = useState(true);
    const [data, setData] = useState([]);

  useEffect(() => {
    fetch('https://api.themoviedb.org/3/movie/upcoming?api_key=659bb1574dddd94cbbe15920e9636d0b&language=en-US&page=21')
      .then((response) => response.json())
      .then((json) => setData(json.results))
      .catch((error) => console.error(error))
      .finally(() => setLoading(false));
  }, []);
  
//""
    return (
        <View style={{ flex: 1, paddingTop:10,paddingLeft:10,paddingRight:10 }}>
             <View style={{ flex: 1, padding: 24 }}>
                {isLoading ? <ActivityIndicator/> : (

                   
                    <FlatList data={data}
                        renderItem={(film)=><FilmItem film={film.item} navigation={navigation} />}
                        keyExtractor={(item)=>item.id.toString}
                        ItemSeparatorComponent={()=><View style={{height:0.5,backgroundColor:'#E5E5E5'}}/>}
                   
                    />
                    
                )}
            </View>
            

        </View>
    )
    
}

export default ComingSoon;

const styles = StyleSheet.create({
    container:{
        flex:1,
    },
	body:{
        height:'100%',
        width:'100%',
    }
})
/*<View>
<Text>Hello Home</Text>
<TouchableOpacity onPress={() =>navigation.toggleDrawer()}><Text style={{ color:'red'}}>Drawer</Text></TouchableOpacity>
</View>

     />
                            
                    </View>

                    <FlatList
                    data={data}
                    keyExtractor={({ id }, index) => id}
                    renderItem={({ item }) => (
                        <Text>{item.title}, {item.releaseYear}</Text>
                    )}

*/