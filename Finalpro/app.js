import React from 'react';
import 'react-native-gesture-handler';
import { StyleSheet, Text, View } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createDrawerNavigator } from "@react-navigation/drawer";
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';

import Splash from './Pages/Splash';
import Login from './Pages/Login';
import Home from './Pages/Home';
import Detail from './Pages/Detail';
import Profile from './Pages/Profile';
import ComingSoon from './Pages/ComingSoon';
import Icon from 'react-native-vector-icons/Ionicons';

const Tabs=createBottomTabNavigator();
const RootStack = createStackNavigator();
const HomeStack = createStackNavigator();
const AuthStack = createStackNavigator();
const ProfileStack=createStackNavigator();
const ComingSoonStack=createStackNavigator();
const Drawer = createDrawerNavigator();

const AuthStackScreen = () => (
  <AuthStack.Navigator>
    <AuthStack.Screen name="Login" component={Login} options={{headerShown: false}} />
  </AuthStack.Navigator> 
);

const ProfileStackScreen = ({navigation}) => (
    <ProfileStack.Navigator>
      <ProfileStack.Screen name="Profile" component={Profile} options={{
        title:'MovieDb',
        headerTintColor: '#fff',
        headerStyle: {
          backgroundColor: '#000'
       },
        headerLeft: () => (
            <Icon.Button name="ios-menu" size={25} backgroundColor="#000" onPress={() => navigation.openDrawer()}></Icon.Button>
        )
        }}/>
    </ProfileStack.Navigator>
);

const ComingSoonStackScreen=({navigation})=>(
  <ComingSoonStack.Navigator>
    <ComingSoonStack.Screen name="ComingSoon" component={ComingSoon} options={{
        title:'MovieDb',
        headerTintColor: '#fff',
        headerStyle: {
          backgroundColor: '#000'
       },
        headerLeft: () => (
            <Icon.Button name="ios-menu" size={25} backgroundColor="#000" onPress={() => navigation.openDrawer()}></Icon.Button>
        )
        }}></ComingSoonStack.Screen>
          <ComingSoonStack.Screen name="Detail" component={Detail} options={{
        title:'MovieDb',
        headerTintColor: '#fff',
        headerStyle: {
          backgroundColor: '#000'
       },
        headerLeft: () => (
            <Icon.Button name="ios-menu" size={25} backgroundColor="#000" onPress={() => navigation.openDrawer()}></Icon.Button>
        )
        }} ></ComingSoonStack.Screen> 
  </ComingSoonStack.Navigator>
);


const HomeStackScreen = ({navigation}) =>(
  <HomeStack.Navigator initialRouteName="Home" >
    <HomeStack.Screen name="Home" component={Home} options={{
        title:'MovieDb',
        headerTintColor: '#fff',
        headerStyle: {
          backgroundColor: '#000'
       },
        headerLeft: () => (
            <Icon.Button name="ios-menu" size={25} backgroundColor="#000" onPress={() => navigation.openDrawer()}></Icon.Button>
        )
        }} ></HomeStack.Screen>
        <HomeStack.Screen name="Detail" component={Detail} options={{
        title:'MovieDb',
        headerTintColor: '#fff',
        headerStyle: {
          backgroundColor: '#000'
       },
        headerLeft: () => (
            <Icon.Button name="ios-menu" size={25} backgroundColor="#000" onPress={() => navigation.openDrawer()}></Icon.Button>
        )
        }} ></HomeStack.Screen> 
  </HomeStack.Navigator>
);

const TabsScreen = () => (
  
  <Tabs.Navigator tabBarOptions={{
    activeTintColor: '#FF0000',
    labelStyle: {
      fontSize: 14, marginBottom:15, fontWeight:'bold'
    },
    style: {
      backgroundColor: '#C4C4C4',
    },
  }}>
      <Tabs.Screen name="Trending" component={HomeStackScreen} />
      <Tabs.Screen name="ComingSoon" component={ComingSoonStackScreen} />
    </Tabs.Navigator>
);

const DrawerScreen = () => (
  <Drawer.Navigator initialRouteName="Home" >
    <Drawer.Screen name="Home" component={TabsScreen}  />
    <Drawer.Screen name="Profile" component={ProfileStackScreen}   />
  </Drawer.Navigator>
);

const App = () => {
  return (  
    <NavigationContainer>
      <RootStack.Navigator initialRouteName="Splash">
        <RootStack.Screen  name="Splash" component={Splash} options={{headerShown: false}} />
        <RootStack.Screen  name="Login" component={AuthStackScreen} options={{headerShown: false}} />
        <RootStack.Screen  name="App" component={DrawerScreen}  options={{headerShown: false}}/>
      </RootStack.Navigator>
    </NavigationContainer>
    
  )
}

export default App

const styles = StyleSheet.create({})
/*


*/